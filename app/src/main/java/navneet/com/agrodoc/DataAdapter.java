package navneet.com.agrodoc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by user on 07-Dec-2017.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<IssueModel> articles=new ArrayList<>();
    private Context context;

    public DataAdapter(ArrayList<IssueModel> articles, Context context) {
        this.context=context;
        this.articles=articles;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.issue_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.issue_name.setText(context.getString(R.string.issuename_lable)+articles.get(i).getIssueName());
        viewHolder.issue_desc.setText(context.getString(R.string.issuedetails_label)+articles.get(i).getIssueDesc());
        viewHolder.issue_symptoms.setText(context.getString(R.string.issuesym_label)+articles.get(i).getIssueSymptoms());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView issue_name,issue_desc,issue_symptoms;


        public ViewHolder(View view) {
            super(view);
            issue_name = (TextView)view.findViewById(R.id.issue_name);
            issue_desc = (TextView)view.findViewById(R.id.issue_desc);
            issue_symptoms = (TextView)view.findViewById(R.id.issue_symptoms);
        }
    }

}
