package navneet.com.agrodoc;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements onDeleteIssue {

    private RecyclerView list_rec;
    private PostAdapter mTimeLineAdapter;
    private ArrayList<TimeLineModel> mDataList = new ArrayList<>();
    private VideoView video_view;
    private Button analyze;
    private RecyclerView added_issues;
    private ArrayList<IssueModel> issueModels = new ArrayList<>();
    private ImageView add_button;
    private DataAdapter dataAdapter;
    private IssueViewModel mWordViewModel;
    private IssueListAdapter adapter;
    private String scanned_data;
    private boolean first_object=false;
    private int first_object_id=0;
    private boolean first_test=false;
    private IssueModel issueModel;
    private TextView welcome_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWordViewModel = ViewModelProviders.of(this).get(IssueViewModel.class);




//        TrackSelection.Factory videoTrackSelectionFactory =
//                new AdaptiveTrackSelection.Factory(bandwidthMeter);
//        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
//
//        simpleExoPlayerView.setPlayer(player);
//
//        player.setPlayWhenReady(shouldAutoPlay);
//
//        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
//
//        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"),
//                mediaDataSourceFactory, extractorsFactory, null, null);
//        player.prepare(mediaSource);

//        video_view=(VideoView)findViewById(R.id.video_view);
//        video_view.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4");
//        MediaController mediaController = new MediaController(this);
//        mediaController.setAnchorView(video_view);
//
//        video_view.setMediaController(mediaController);
//        video_view.requestFocus();
//        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            // Close the progress bar and play the video
//            public void onPrepared(MediaPlayer mp) {
//                video_view.start();
//            }
//        });


        add_button = (ImageView) findViewById(R.id.add_button);
        welcome_text = (TextView) findViewById(R.id.welcome_text);
        added_issues = (RecyclerView) findViewById(R.id.added_issues);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        added_issues.setLayoutManager(linearLayoutManager);
        adapter = new IssueListAdapter(this, this);
        added_issues.setAdapter(adapter);

        analyze = (Button) findViewById(R.id.cam_button);
        list_rec = (RecyclerView) findViewById(R.id.list_rec);
        list_rec.setLayoutManager(new LinearLayoutManager(this));
        list_rec.setHasFixedSize(true);

        Intent intent = getIntent();

        if (intent != null) {
            String id=intent.getStringExtra("first_object_id");
            if (id!=null) {
                scanned_data = intent.getStringExtra("scanned_data");
                IssueModel issueModel = new IssueModel("Initial Scan Result", scanned_data, "updated symptoms");
                issueModel.setId(Integer.parseInt(id));
                issueModel.setIssueState("1");
                mWordViewModel.update(issueModel);
                first_test=true;
            }
        }


        mWordViewModel.getAllWords().observe(this, new Observer<List<IssueModel>>() {
            @Override
            public void onChanged(@Nullable final List<IssueModel> issueModels) {
                // Update the cached copy of the words in the adapter.
                if (issueModels.size() > 0) {
                    add_button.setVisibility(View.GONE);
                    welcome_text.setVisibility(View.GONE);
                    analyze.setEnabled(true);
                    issueModel=issueModels.get(0);
                    first_object_id=issueModels.get(0).getId();
                    first_object=true;
                    added_issues.setVisibility(View.VISIBLE);
                    list_rec.setVisibility(View.VISIBLE);
                } else {
                    add_button.setVisibility(View.VISIBLE);
                    welcome_text.setVisibility(View.VISIBLE);
                    analyze.setEnabled(false);
                }
                adapter.setIssues(issueModels);
            }
        });


        populateList();

        analyze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (first_object) {
                    Intent intent1 = new Intent(MainActivity.this, CameraActivity.class);
                    intent1.putExtra("first_object_id", String.valueOf(first_object_id));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);
                }
            }
        });

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPosterDetails();
            }
        });
    }

    public void loadPosterDetails() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View resetDialog = getLayoutInflater().inflate(R.layout.details_dialog, null);

        TextView title = (TextView) resetDialog.findViewById(R.id.title);
        Button ok = (Button) resetDialog.findViewById(R.id.ok_button);
//        Button cancel=(Button)resetDialog.findViewById(R.id.cancel_button);
        EditText issue_name = (EditText) resetDialog.findViewById(R.id.issue_name);
        EditText issue_desc = (EditText) resetDialog.findViewById(R.id.issue_desc);
        TextView issue_symptoms = (TextView) resetDialog.findViewById(R.id.issue_symptoms);


        issue_name.setText("tomato early blight");
        issue_desc.setText("leaf round circles");
        issue_symptoms.setText("yellow leaf, round marks");

        mBuilder.setView(resetDialog);
        AlertDialog alertDialog = mBuilder.create();
        alertDialog.show();


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(issue_name.getText())) {
                    issue_name.setError("Issue name can't be empty!");
                } else {
                    IssueModel issueModel = new IssueModel(issue_name.getText().toString(), issue_desc.getText().toString(), issue_symptoms.getText().toString());
//                    issueModels.add(issueModel);
                    mWordViewModel.insert(issueModel);
//                    dataAdapter=new DataAdapter(issueModels,MainActivity.this);
//                    added_issues.setAdapter(dataAdapter);
                    alertDialog.dismiss();
                }
            }
        });

//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });

    }

    private void populateList() {
        mDataList.add(new TimeLineModel("Scan initial leaf sample", "", (first_test) ? OrderStatus.COMPLETED : OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("Run primary tests on sample", "2017-02-12 08:00", OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("View primary test results", "2017-02-11 21:00", OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("Scan secondary sample", "2017-02-11 18:00", OrderStatus.INACTIVE));
        mDataList.add(new TimeLineModel("Secondary sample results", "2017-02-11 09:30", OrderStatus.INACTIVE));
//        mDataList.add(new TimeLineModel("Order is being readied for dispatch", "2017-02-11 08:00", OrderStatus.COMPLETED));
//        mDataList.add(new TimeLineModel("Order processing initiated", "2017-02-10 15:00", OrderStatus.COMPLETED));
//        mDataList.add(new TimeLineModel("Order confirmed by seller", "2017-02-10 14:30", OrderStatus.COMPLETED));
//        mDataList.add(new TimeLineModel("Order placed successfully", "2017-02-10 14:00", OrderStatus.COMPLETED));

        mTimeLineAdapter = new PostAdapter(mDataList, this);
        list_rec.setAdapter(mTimeLineAdapter);
    }

    @Override
    public void onDeleteIssueClicked(IssueModel issueModel) {
//        IssueModel issueModel=new IssueModel("empty","empty","empty");
        mWordViewModel.delete(issueModel);
        list_rec.setVisibility(View.GONE);
        added_issues.setVisibility(View.GONE);
    }
}
