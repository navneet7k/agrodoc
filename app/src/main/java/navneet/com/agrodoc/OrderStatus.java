package navneet.com.agrodoc;


public enum OrderStatus {

    COMPLETED,
    ACTIVE,
    INACTIVE;

}
