package navneet.com.agrodoc;

/**
 * Created by Navneet Krishna on 10/11/18.
 */
public interface onDeleteIssue {
    void onDeleteIssueClicked(IssueModel issueModel);
}
